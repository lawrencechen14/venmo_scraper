import requests
import csv
import pandas as pd
import sys

if __name__ == '__main__':

    # Specify filename of the csv to add data to
    filename = sys.argv[1]

    # Specify URL of Venmo API which we will scrape from.
    url = "https://venmo.com/api/v5/public"

    try:
        # Get data from the Venmo API and change it to a JSON.
        data = requests.get(url).json()

        # Create a dataframe from the JSON, and append it to our current dataframe.
        df = pd.DataFrame.from_dict(data["data"])

        # Open file at csv and write new rows from our latest data pull.
        with open(filename, 'a') as f:
            for index, row in df.iterrows():
                writer = csv.writer(f)
                writer.writerow(row)

        print("SUCCESS: Data successfully scraped")

    except KeyError:
        # Except when the the API does not give us any data due to scraping limits.
        print("FAILURE: No data at this time")

