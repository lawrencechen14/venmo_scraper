import requests
import pandas as pd
import time
import sys

if __name__ == '__main__':

    # Get number of hours as argument, and the name of the file to insert data to as a second argument.
    num_hours = int(sys.argv[1])
    path_to_folder = sys.argv[2]
    file_name = sys.argv[3]

    # Specify URL of Venmo API which we will scrape from.
    url = "https://venmo.com/api/v5/public"

    # Create new dataframe to populate.
    df = pd.DataFrame()

    # Specify how long we want the scraper to run, which is changed by num_hours.
    time_to_run_in_hours = num_hours
    time_to_run_in_minutes = 60 * time_to_run_in_hours
    time_to_run_in_seconds = 60 * time_to_run_in_minutes

    # Create end time from the current time plus however long we want the scraper to run.
    start_time = time.time()
    end_time = start_time + time_to_run_in_seconds

    # Initialize number of passes to 1 to keep track of how many API calls we do in a session.
    num_passes = 0
    file_number = 0

    print("-----------------------")
    print("Starting Venmo Scraping")
    print("-----------------------")

    while end_time > time.time():
        # Print the pass number that we are on.
        num_passes += 1
        print("Pass", num_passes)

        # Try to scrape data
        try:
            # Get data from the Venmo API and change it to a JSON.
            data = requests.get(url).json()

            # Create a dataframe from the JSON, and append it to our current dataframe.
            sub_df = pd.DataFrame.from_dict(data["data"])
            df = df.append(sub_df)

            # Wait for 30 seconds, otherwise we will get an error for too many API calls at a time.
            time.sleep(30)

        except KeyError:
            print("-----------------------")
            print("No data available, storing data so far")

            # Store data at certain file path.
            file_path = path_to_folder + "/" + file_name + "_" + str(file_number) + ".csv"
            df.to_csv(file_path)

            print("Data stored into", file_path)

            # Reset df and num passes, and increment file number.
            df = pd.DataFrame()
            num_passes = 0
            file_number += 1

            print("Restarting Venmo Scraping")
            print("-----------------------")

            # Wait for 30 seconds, otherwise we will get an error for too many API calls at a time.
            time.sleep(30)

    file_path = path_to_folder + "/" + file_name + "_" + str(file_number) + ".csv"
    df.to_csv(file_path)
